package org.sure.sjson;

public class JsonCompileException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public JsonCompileException() {
		super();
	}

	public JsonCompileException(String message) {
		super(message);
	}

	public JsonCompileException(String message, Throwable ta) {

		super(message, ta);
	}

	public JsonCompileException(Throwable ta) {
		super(ta);
	}

}
