package org.sure.sjson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class Json {

	public static Json compile(String json) throws JsonCompileException {
		if (json == null) {
			throw new NullPointerException("json input is null");
		}
		return getJson(json);
	}

	static Json getJson(String json) throws JsonCompileException {
		json = json.trim();
		if (JsonObject.test(json)) {
			return new JsonObject(json);
		}
		if (JsonNumber.test(json)) {
			return new JsonNumber(json);
		}
		if (JsonArray.test(json)) {
			return new JsonArray(json);
		}
		if (JsonBoolean.test(json)) {
			return new JsonBoolean(json);
		}
		if (JsonString.test(json)) {
			return new JsonString(json);
		}
		if (JsonNull.test(json)) {
			return new JsonNull(json);
		}
		throw new JsonCompileException("unknown json format: " + json);
	}

	protected static List<String> split(String jsonstr, char delimiter) {
		return split(jsonstr, delimiter, 1, jsonstr.length() - 1);
	}

	protected static List<String> split(String jsonstr, char delimiter,
			int start, int end) {
		List<String> sublist = new ArrayList<String>();
		int lastpoint = start;
		int brackets = 0;
		int quotation = 0;
		for (int i = start; i < end; i++) {
			char jc = jsonstr.charAt(i);
			if (quotation == 1 && jc == '\\') {
				i++;
				continue;
			}
			if (jc == '\"') {
				quotation = 1 - quotation;
				continue;
			}
			if (quotation == 1) {
				continue;
			}
			if (jc == '{' || jc == '[') {
				brackets++;
				continue;
			}
			if (jc == '}' || jc == ']') {
				brackets--;
				continue;
			}
			if (brackets != 0) {
				continue;
			}
			if (jc == delimiter) {
				sublist.add(jsonstr.substring(lastpoint, i).trim());
				lastpoint = i + 1;
			}
		}
		sublist.add(jsonstr.substring(lastpoint, end).trim());

		return sublist;

	}

	protected String json;

	private Json parent;

	protected Json() {
		init();
	}

	protected Json(String json) {
		init();
		setJson(json);
	}

	public abstract Json get(int index);

	public abstract Json get(String key);

	public abstract boolean getBoolean();

	public Json getJsonByPath(String path) {
		path = path.trim();
		if (path.startsWith("/")) {
			path = path.substring(1);
		}
		if (path.endsWith("/")) {
			path = path.substring(0, path.length() - 1);
		}
		String paths[] = path.split("/");
		Json temp = this;
		for (int i = 0; i < paths.length; i++) {
			if ("..".equals(paths[i])) {
				temp = temp.getParent();
			} else if (".".equals(paths[i])) {
				// do nothing
			} else {
				temp = temp.get(paths[i]);
			}
			if (temp == null) {
				return null;
			}
		}

		return temp;
	}

	public String getJsonStr() {
		return json;
	}

	public abstract double getNumber();

	public Json getParent() {
		return this.parent;
	}

	public abstract String getString();

	protected void init() {

	}

	public boolean isArray() {
		return false;
	}

	public boolean isBoolean() {
		return false;
	}

	public boolean isNull() {
		return false;
	}

	public boolean isNumber() {
		return false;
	}

	public boolean isObject() {
		return false;
	}

	public boolean isString() {
		return false;
	}

	protected Json parse() {
		return parse(this.json);
	}

	protected abstract Json parse(String json);

	protected void setJson(String json) {
		this.json = json.trim();
		parse();
	}

	public void setParent(Json json) {
		this.parent = json;
	}

	public abstract Appendable toJson(Appendable buff) throws IOException;

	@Override
	public String toString() {
		StringBuilder buff = new StringBuilder();
		try {
			toJson(buff);
		} catch (IOException e) {
			// never
		}
		return buff.toString();
	}
}
