package org.sure.sjson;

import java.io.IOException;

public class JsonNull extends Json {

	public static boolean test(String json) {
		return "null".equals(json);
	}

	public JsonNull() {

	}

	protected JsonNull(String json) {
		
	}

	@Override
	public Json get(int index) {
		throw new JsonValueTypeException("can not get Json by index from null");
	}

	@Override
	public Json get(String key) {
		throw new JsonValueTypeException("can not get Json by key from null");
	}

	@Override
	public boolean getBoolean() {
		return false;
	}

	@Override
	public double getNumber() {
		return 0;
	}

	@Override
	public String getString() {
		return null;
	}

	@Override
	public boolean isNull() {
		return true;
	}

	@Override
	protected Json parse(String json) {
		return this;
	}

	@Override
	public Appendable toJson(Appendable buff) throws IOException {
		return buff.append("null");
	}

	@Override
	public String toString() {
		return "null";
	}

}
