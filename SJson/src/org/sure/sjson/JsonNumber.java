package org.sure.sjson;

import java.io.IOException;
import java.util.regex.Pattern;

public class JsonNumber extends Json {

	private static Pattern numberPa = Pattern
			.compile("^[-|+]?\\d+(\\.\\d+)?([e|E][-|+]?\\d+)?$");

	public static boolean test(String json) {
		return numberPa.matcher(json).matches();
	}

	private double value;

	public JsonNumber(double value) {
		setValue(value);
	}

	protected JsonNumber(String json) {
		super(json);
	}

	@Override
	public Json get(int index) {
		throw new JsonValueTypeException(
				"can not get Json by index from a number");
	}

	@Override
	public Json get(String key) {
		throw new JsonValueTypeException(
				"can not get Json by key from a number");
	}

	@Override
	public boolean getBoolean() {
		return value != 0;
	}

	@Override
	public double getNumber() {
		return value;
	}

	@Override
	public String getString() {
		return Double.toString(value);

	}

	@Override
	public boolean isNumber() {
		return true;
	}

	@Override
	protected Json parse(String json) {
		value = Double.parseDouble(json);
		return this;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public Appendable toJson(Appendable buff) throws IOException {
		return buff.append(Double.toString(value));
	}

	@Override
	public String toString() {
		return Double.toString(value);
	}

}
