package org.sure.sjson;

import java.io.IOException;

public class JsonString extends Json {

	static String quoteRemove(String str) {
		return test(str) ? str.substring(1, str.length() - 1) : str;
	}

	public static boolean test(String json) {
		return json.length() >= 2 && json.startsWith("\"")
				&& json.endsWith("\"");
	}

	private String value;

	protected JsonString(String json) {
		super(json);
	}

	public JsonString(String value, int flag) {
		setValue(value);
	}

	@Override
	public Json get(int index) {
		throw new JsonValueTypeException(
				"can not get Json by index from a string");
	}

	@Override
	public Json get(String key) {
		throw new JsonValueTypeException(
				"can not get Json by key from a string");
	}

	@Override
	public boolean getBoolean() {
		return !"".equals(value);
	}

	@Override
	public double getNumber() {
		return Double.parseDouble(value);
	}

	@Override
	public String getString() {
		return value;
	}

	@Override
	public boolean isString() {
		return true;
	}

	@Override
	protected Json parse(String json) {
		this.value = quoteRemove(this.json).replaceAll("\\\"", "\"");
		return this;
	}

	public void setValue(String value) {
		if (value == null) {
			throw new JsonCompileException("null");
		}
	}

	@Override
	public Appendable toJson(Appendable buff) throws IOException {
		return buff.append("\"").append(value).append("\"");
	}
}
