package org.sure.sjson;

public class JsonValueTypeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public JsonValueTypeException() {
		super();
	}

	public JsonValueTypeException(String message) {
		super(message);
	}

	public JsonValueTypeException(String message, Throwable ta) {
		super(message, ta);
	}

	public JsonValueTypeException(Throwable ta) {
		super(ta);
	}

}
