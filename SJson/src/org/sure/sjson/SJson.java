package org.sure.sjson;

public interface SJson {

	public boolean parseJson(Json json);
	
	public StringBuilder toJson(StringBuilder buff);
	
}
