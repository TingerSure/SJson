package org.sure.sjson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsonArray extends Json {

	public static boolean test(String json) {
		return json.startsWith("[") && json.endsWith("]");
	}

	private List<Json> values;

	public JsonArray() {
		this("[]");
	}

	protected JsonArray(String json) {
		super(json);
	}

	@Override
	public Json get(int index) {
		return values.get(index);
	}

	@Override
	public Json get(String key) {
		try {
			int index = Integer.parseInt(key);
			return get(index);
		} catch (NumberFormatException e) {
			throw new JsonValueTypeException(
					"Only Number key can get Json from an array, such as \"1\".");
		}
	}

	@Override
	public boolean getBoolean() {
		return true;
	}

	@Override
	public double getNumber() {
		if (length() == 1) {
			Json num = values.get(0);
			if (num.isNumber()) {
				return num.getNumber();
			}
		}
		throw new JsonValueTypeException("this json is not a number");
	}

	@Override
	public String getString() {
		if (length() == 1) {
			Json num = values.get(0);
			if (num.isString()) {
				return num.getString();
			}
		}
		throw new JsonValueTypeException("this json is not a number");
	}

	public List<Json> getValues() {
		return values;
	}

	@Override
	protected void init() {
		values = new ArrayList<Json>();
	}

	@Override
	public boolean isArray() {
		return true;
	}

	public int length() {
		return values.size();
	}

	@Override
	protected Json parse(String json) {
		List<String> substrs = split(json, ',');
		for (int i = 0; i < substrs.size(); i++) {
			String substr = substrs.get(i).trim();
			if (!"".equals(substr)) {
				Json temp = getJson(substr);
				values.add(temp);
				temp.setParent(this);
			}
		}
		return this;
	}

	@Override
	public Appendable toJson(Appendable buff) throws IOException {
		buff.append("[");
		for (int i = 0; i < values.size(); i++) {
			if (i != 0) {
				buff.append(",");
			}
			values.get(i).toJson(buff);
		}
		return buff.append("]");

	}

}
