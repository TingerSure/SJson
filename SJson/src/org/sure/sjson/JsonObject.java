package org.sure.sjson;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class JsonObject extends Json {

	private static Pattern keyPa = Pattern.compile("^[_a-zA-Z][_a-zA-Z0-9]*$");

	public static boolean test(String json) {
		return json.startsWith("{") && json.endsWith("}");
	}

	private Map<String, Json> values;

	public JsonObject() {
		this("{}");
	}

	protected JsonObject(String json) {
		super(json);
	}

	@Override
	public Json get(int index) {
		return values.get(Integer.toString(index));
	}

	@Override
	public Json get(String key) {
		return values.get(key);
	}

	@Override
	public boolean getBoolean() {
		return true;
	}

	public Set<String> getKeySet() {
		return values.keySet();
	}

	@Override
	public double getNumber() {
		throw new JsonValueTypeException("can not get number from an object");
	}

	@Override
	public String getString() {
		throw new JsonValueTypeException("can not get string from an object");

	}

	public Map<String, Json> getValues() {
		return values;
	}

	@Override
	protected void init() {
		values = new HashMap<String, Json>();
	}

	@Override
	public boolean isObject() {
		return true;
	}

	@Override
	protected Json parse(String json) {
		List<String> substrs = split(json, ',');
		for (int i = 0; i < substrs.size(); i++) {
			String substr = substrs.get(i).trim();
			if ("".equals(substr)) {
				continue;
			}
			List<String> keyvalues = split(substr, ':', 0, substr.length());
			if (keyvalues.size() != 2) {
				throw new JsonCompileException("json format error: " + substr
						+ " in Object " + json);
			}

			String key = keyvalues.get(0);

			if (!JsonString.test(key) && !keyPa.matcher(key).matches()) {
				throw new JsonCompileException(
						"json format error: unusable characters in the key: \""
								+ key + "\" in Object " + json);
			}
			Json temp = getJson(keyvalues.get(1));
			if (null != values.put(JsonString.quoteRemove(key), temp)) {
				throw new JsonCompileException(
						"json format error: duplicate key : \"" + key
								+ "\" in Object " + json);
			}
			temp.setParent(this);
		}

		return this;
	}

	@Override
	public Appendable toJson(Appendable buff) throws IOException {
		buff.append("{");
		Iterator<String> it = getKeySet().iterator();
		for (int i = 0; it.hasNext(); i++) {
			if (i != 0) {
				buff.append(",");
			}
			String key = it.next();
			buff.append("\"").append(key).append("\":");
			values.get(key).toJson(buff);
		}
		return buff.append("}");
	}

}
