package org.sure.sjson;

import java.io.IOException;

public class JsonBoolean extends Json {

	public static boolean test(String json) {
		return "false".equals(json) || "true".equals(json);
	}

	private boolean value;

	public JsonBoolean(boolean value) {
		this.value = value;
	}

	protected JsonBoolean(String json) {
		super(json);
	}

	@Override
	public Json get(int index) {
		throw new JsonValueTypeException(
				"can not get Json by index from boolean");
	}

	@Override
	public Json get(String key) {
		throw new JsonValueTypeException("can not get Json by key from boolean");
	}

	@Override
	public boolean getBoolean() {
		return value;
	}

	@Override
	public double getNumber() {
		return value ? 1 : 0;
	}

	@Override
	public String getString() {
		return value ? "true" : "false";
	}

	@Override
	public boolean isBoolean() {
		return true;
	}

	@Override
	protected Json parse(String json) {
		value = "true".equals(this.json);
		return this;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	@Override
	public Appendable toJson(Appendable buff) throws IOException {
		return buff.append(getString());
	}

	@Override
	public String toString() {
		return getString();
	}

}
